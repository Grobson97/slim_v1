import gdspy
import numpy as np


class EdgeBondPad:
    def __init__(
        self,
        pad_width: float,
        pad_gap: float,
        cpw_width: float,
        cpw_gap: float,
        pad_length: float,
        transition_length: float,
        origin_x=0.0,
        origin_y=0.0,
    ) -> None:
        """
        Creates a new instance of a bond pad.

        :param pad_width: Width of cpw at the bond pad.
        :param pad_gap: Gap of cpw at the bond pad.
        :param cpw_width: Width of CPW centre line joining the bond pad.
        :param cpw_width: Gap of CPW centre line joining the bond pad.
        :param pad_length: Length of bond pad section.
        :param transition_length: Length of pad to cpw transition.
        :param origin_x: x coordinate for the centre line of symmetry for the bond pad. Oriented such that the origin
        lies on the edge of the device. Default orientation points down.
        :param origin_y: y coordinate for the edge of the chip.
        """

        self.pad_width = pad_width
        self.pad_gap = pad_gap
        self.cpw_width = cpw_width
        self.cpw_gap = cpw_gap
        self.pad_length = pad_length
        self.transition_length = transition_length
        self.origin_x = origin_x
        self.origin_y = origin_y

    def make_cell(
        self,
        cpw_centre_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        cell_name="Bond Pad",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given BongPad instance.

        :param: cpw_centre_layer: GDSII layer for the microstrip.
        :param: ground_layer: GDSII layer for the ground plane.
        :param: dielectric_layer: GDSII layer for the dielectric.
        :param: cell_name: Name to be used to reference the cell.
        """

        # Create bond pad cell to add geometries to:
        bond_pad_cell = gdspy.Cell(cell_name)

        bond_pad_centre_path = gdspy.Path(
            width=self.pad_width, initial_point=(self.origin_x, self.origin_y - 100)
        )
        bond_pad_centre_path.segment(
            length=self.pad_length, direction="-y", layer=cpw_centre_layer
        )
        bond_pad_centre_path.segment(
            length=self.transition_length,
            direction="-y",
            final_width=self.cpw_width,
            layer=cpw_centre_layer,
        )

        bond_pad_gnd_path = gdspy.Path(
            width=self.pad_width + 2 * self.pad_gap,
            initial_point=(self.origin_x, self.origin_y),
        )
        bond_pad_gnd_path.segment(
            length=self.pad_length + 100, direction="-y", layer=ground_layer
        )
        bond_pad_gnd_path.segment(
            length=self.transition_length,
            direction="-y",
            final_width=self.cpw_width + 2 * self.cpw_gap,
            layer=ground_layer,
        )

        # Add hole in dielectric:
        dielectric_hole = gdspy.Rectangle(
            point1=(self.origin_x - self.pad_width / 2 - 10, self.origin_y - 90),
            point2=(
                self.origin_x + self.pad_width / 2 + 10,
                self.origin_y - self.pad_length - 10,
            ),
            layer=dielectric_layer,
        )

        bond_pad_cell.add([bond_pad_centre_path, bond_pad_gnd_path, dielectric_hole])

        return bond_pad_cell

    def get_feedline_connection_x(self) -> float:
        """
        Function to return the x coordinates at which to connect the feedline to
        the bond pad.
        """

        return self.origin_x

    def get_feedline_connection_y(self) -> float:
        """
        Function to return the y coordinates at which to connect the feedline to
        the bond pad.
        """

        return self.origin_y - self.pad_length - self.transition_length - 100
