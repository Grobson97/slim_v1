import gdspy
from models.pp_lekid_coupler import PPLekidCoupler
from models.pp_capacitor import PPCapacitor
from models.inductor import Inductor


class PPLekid:
    def __init__(
        self,
        target_f0: float,
        signal_input_x=0.0,
        signal_input_y=0.0,
    ) -> None:
        """
        Creates new instance of a single Lekid with an interdigital capacitor
        using the modules for the coupler, inductor and idc.

        :param float target_f0: Target resonant frequency in Hz for the Lekid. Currently there is no algorithm, so
        directly set length as the f0.
        :param signal_input_x: X-coordinate of the connection to whatever
        is delivering the signal to the Lekid inductor.
        :param signal_input_y: Y-coordinate of the connection to whatever
        is delivering the signal to the Lekid inductor.
        """

        self.target_f0 = target_f0

        # NB: If the following interpolation models change you also need to change util.lekid_coupler_length() and
        # util.lekid_idc_length()
        self.capacitor_length = (
            -3.742e-25 * target_f0**3
            + 3.079e-15 * target_f0**2
            - 8.98e-6 * target_f0
            + 9796
        )

        self.coupler_width = (
            5.11e-5 * self.capacitor_length**2
            - 0.07818 * self.capacitor_length
            + 52.74
        )

        self.signal_input_x = signal_input_x
        self.signal_input_y = signal_input_y

        self.inductor = Inductor(
            track_width=2.0,
            bend_radius=6.0,
            signal_input_connection_x=signal_input_x,
            signal_input_connection_y=signal_input_y,
            tolerance=0.01,
        )

        self.pp_capacitor = PPCapacitor(
            capacitor_length=self.capacitor_length,
            inductor_connection_x=self.inductor.get_idc_connection_x(),
            inductor_connection_y=self.inductor.get_idc_connection_y() - 10.0,
        )

        self.lekid_coupler = PPLekidCoupler(
            plate_width=self.coupler_width,
            plate_height=15.0,
            plate_gap=5.0,
            ground_hole_width=221.0,
            coupler_track_width=6.0,
            length_to_capacitor=60.0,
            length_to_readout=71.0,
            lekid_capacitor_connection_x=self.pp_capacitor.get_coupler_connection_x(),
            lekid_capacitor_connection_y=self.pp_capacitor.get_coupler_connection_y(),
        )

    def make_cell(
        self,
        inductor_layer: int,
        device_plate_layer: int,
        readout_layer: int,
        ground_layer: int,
        readout_width: float,
        readout_gap: float,
        cell_name="Lekid",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given Lekid geometry.
        The geometry is oriented such that coordinates of the connection to the
        readout line has the largest y value and the connection to the signal input
        the lowest.

        :param inductor_layer: GDSII layer for inductor geometry.
        :param device_plate_layer: GDSII layer for device layer plate capacitor geometry.
        :param readout_layer: GDSII layer for readout line geometry.
        :param ground_layer: GDSII layer for ground plane capacitor islands
        geometry.
        :param dielectric_layer: GDSII layer for the dielectric layer.
        :param readout_width: Width of CPW readout center line.
        :param readout_gap: CPW gap width for readout line.
        :param cell_name: Name to be used to reference the cell. If creating
        multiple differing LEKIDS give cell names as LEKID_N where N is a unique
        number.
        """

        # Create Lekid cell to add module cells to.
        lekid_cell = gdspy.Cell(cell_name)

        cell_id = "0"
        if cell_name != "Lekid":
            cell_id = cell_name.split("_")[1]

        # Make gdspy cells:
        inductor_cell = self.inductor.make_cell(
            inductor_layer, cell_name=cell_name + " Inductor_" + cell_id
        )
        idc_cell = self.pp_capacitor.make_cell(
            plate_layer=device_plate_layer,
            ground_layer=ground_layer,
            cell_name=cell_name + " PP_Capacitor_" + cell_id,
        )
        lekid_coupler_cell = self.lekid_coupler.make_cell(
            plate_layer=device_plate_layer,
            ground_layer=ground_layer,
            cell_name=cell_name + " PP_Lekid_Coupler_" + cell_id,
        )

        # Create reference cells of each sub-structure.
        inductor_ref_cell = gdspy.CellReference(inductor_cell, (0, 0))
        idc_ref_cell = gdspy.CellReference(idc_cell, (0, 0))
        elbow_coupler_ref_cell = gdspy.CellReference(lekid_coupler_cell, (0, 0))

        readout_path_points = [
            (
                self.lekid_coupler.get_readout_connection_x() - 80,
                self.lekid_coupler.get_readout_connection_y() + readout_width / 2,
            ),
            (
                self.lekid_coupler.get_readout_connection_x() + 60,
                self.lekid_coupler.get_readout_connection_y() + readout_width / 2,
            ),
        ]

        readout_centre_path = gdspy.FlexPath(
            points=readout_path_points,
            width=readout_width,
            offset=0.0,
            corners="circular bend",
            bend_radius=200,
            max_points=1999,
            layer=readout_layer,
        )

        readout_gap_path = gdspy.FlexPath(
            points=readout_path_points,
            width=readout_width + 2 * readout_gap,
            offset=0.0,
            corners="circular bend",
            bend_radius=200,
            max_points=1999,
            layer=ground_layer,
        )

        bridge = gdspy.Rectangle(
            (
                self.lekid_coupler.get_readout_connection_x() - 60,
                self.lekid_coupler.get_readout_connection_y()
                + readout_gap
                + readout_width,
            ),
            (
                self.lekid_coupler.get_readout_connection_x() - 54,
                self.lekid_coupler.get_readout_connection_y() - readout_gap,
            ),
            layer=10,
        )

        readout_gap_path = gdspy.boolean(
            operand1=readout_gap_path,
            operand2=bridge,
            operation="not",
            layer=ground_layer,
        )

        # Add reference cells to single_filter_cell.
        lekid_cell.add(
            [
                inductor_ref_cell,
                idc_ref_cell,
                elbow_coupler_ref_cell,
                readout_centre_path,
                readout_gap_path,
            ]
        )

        return lekid_cell

    def get_readout_connections(self, readout_width: float) -> list:
        """
        Function to return the coordinates of the two sparse_lekid readout connections in the form [(x1, y1), (x2, y2)]. Here
        x1 and y1 correspond to the LH connection, and x2 and y2 the RH connection in the default orientation.

        :param readout_width: Width of centre line of the readout CPW.
        """

        x1 = self.lekid_coupler.get_readout_connection_x() - 80
        y = self.lekid_coupler.get_readout_connection_y() + readout_width / 2
        x2 = self.lekid_coupler.get_readout_connection_x() + 60

        return [(x1, y), (x2, y)]
