import gdspy
from gdspy.library import Cell
import numpy as np


def is_even(number: int):
    if (number % 2) == 0:
        return True
    if (number % 2) != 0:
        return False


class CPWStub:
    def __init__(
        self,
        cpw_centre_width: float,
        cpw_gap: float,
        stub_length: float,
        stub_width: float,
        readout_connection_x=0.0,
        readout_connection_y=0.0,
    ) -> None:
        """
        Creates new instance of a cpw stub to mimic the elbow coupler of a LEKID, including a section of cpw with a
        bridge.

        :param cpw_centre_width: Width of CPW centre line.
        :param cpw_gap: gap width of cpw.
        :param stub_length: Length of the coupler fingers.
        :param stub_width: Width of the coupler fingers.
        :param readout_connection_x: X coordinate of center of readout connection. Default= 0.0.
        :param readout_connection_y: Y coordinate of center of readout connection. Default= 0.0.
        """

        self.cpw_centre_width = cpw_centre_width
        self.cpw_gap = cpw_gap
        self.stub_length = stub_length
        self.stub_width = stub_width
        self.readout_connection_x = readout_connection_x
        self.readout_connection_y = readout_connection_y

    def make_cell(
        self,
        cpw_center_layer: int,
        ground_layer: int,
        cell_name="CPW Stub",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given cpw stub instance. The geometry is
        oriented such that the stub is perpendicular to the x axis
        and the coupler hangs down from the cpw.

        :param int cpw_center_layer: GDSII layer for the readout feedline.
        :param ground_layer: GDSII layer for ground plane.
        :param cell_name: Name to be given to the cell.
        """

        cpw_centre_width = self.cpw_centre_width
        cpw_gap = self.cpw_gap
        stub_length = self.stub_length
        stub_width = self.stub_width
        readout_connection_x = self.readout_connection_x
        readout_connection_y = self.readout_connection_y

        # Create cell to add coupler geometry to
        stub_cell = gdspy.Cell(cell_name)

        stub = gdspy.Rectangle(
            (
                readout_connection_x + 80.0 - stub_width / 2,
                readout_connection_y - cpw_centre_width / 2,
            ),
            (
                readout_connection_x + 80 + stub_width / 2,
                readout_connection_y - cpw_centre_width / 2 - stub_length,
            ),
            layer=cpw_center_layer,
        )

        readout_path_points = [
            (
                readout_connection_x,
                readout_connection_y,
            ),
            (
                readout_connection_x + 140,
                readout_connection_y,
            ),
        ]

        readout_centre_path = gdspy.FlexPath(
            points=readout_path_points,
            width=cpw_centre_width,
            offset=0.0,
            corners="circular bend",
            bend_radius=200,
            max_points=1999,
            layer=cpw_center_layer,
        )

        readout_gap_path = gdspy.FlexPath(
            points=readout_path_points,
            width=cpw_centre_width + 2 * cpw_gap,
            offset=0.0,
            corners="circular bend",
            bend_radius=200,
            max_points=1999,
            layer=ground_layer,
        )

        bridge = gdspy.Rectangle(
            (
                readout_connection_x + 20,
                readout_connection_y + cpw_gap + cpw_centre_width / 2,
            ),
            (
                readout_connection_x + 26,
                readout_connection_y - cpw_gap - cpw_centre_width / 2,
            ),
            layer=10,
        )

        readout_gap_path = gdspy.boolean(
            operand1=readout_gap_path,
            operand2=bridge,
            operation="not",
            layer=ground_layer,
        )

        stub_cell.add([stub, readout_centre_path, readout_gap_path])

        return stub_cell

    def get_readout_output_connection_x(self) -> float:
        """
        Function to return the x-coordinate of readout output line. in the default orientation this is the right hand
        connection (port).
        """

        return self.readout_connection_x + 140.0

    def get_readout_output_connection_y(self) -> float:
        """
        Function to return the y-coordinate of readout output line. in the default orientation this is the right hand
        connection (port).
        """

        return self.readout_connection_y
