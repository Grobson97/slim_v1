import gdspy
from models.microstrip_step_over import MicrostripStepOver


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_step_over = MicrostripStepOver(
        microstrip_width=2.0, centre_x=0.0, centre_y=0.0
    )

    test_step_over_cell = test_step_over.make_cell(
        main_microstrip_layer=0,
        ground_layer=1,
        dielectric_bridge_layer=2,
        cell_name="Step Over",
    )

    main_cell.add(test_step_over_cell)

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
