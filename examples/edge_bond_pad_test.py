import gdspy
from models.edge_bond_pad import EdgeBondPad
import util.filter_bank_builder_tools as tools
import numpy as np


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    bond_pad = EdgeBondPad(
        pad_width=800,
        pad_gap=500,
        cpw_width=16,
        cpw_gap=10,
        pad_length=360,
        transition_length=1500,
        origin_x=0.0,
        origin_y=0.0,
    )
    bond_pad_cell = bond_pad.make_cell(
        cpw_centre_layer=0,
        ground_layer=1,
        dielectric_layer=2,
        cell_name="Bond Pad",
    )

    rotation = 90

    bond_pad_origin_x = 0
    bond_pad_origin_y = 0
    bond_pad_reference = gdspy.CellReference(
        bond_pad_cell, (bond_pad_origin_x, bond_pad_origin_y), rotation=rotation
    )

    connection_x = bond_pad.get_feedline_connection_x() + bond_pad_origin_x
    connection_y = bond_pad.get_feedline_connection_y() + bond_pad_origin_y
    print(connection_x, connection_y)

    print(
        tools.rotate_coordinates(
            bond_pad.get_feedline_connection_x(),
            bond_pad.get_feedline_connection_y(),
            angle=rotation / 360 * 2 * np.pi,
        )
    )

    main_cell.add(bond_pad_reference)

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
